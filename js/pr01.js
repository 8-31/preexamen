document.getElementById('btnBuscar').addEventListener('click', function() {
    // Obtener el valor del campo de entrada
    var idEmpleado = document.getElementById('idEmpleado').value;

    // Verificar si el campo de entrada no está vacío
    if (idEmpleado.trim() !== '') {
        // Hacer la solicitud a la API con el ID del empleado utilizando Axios
        axios.get('https://jsonplaceholder.typicode.com/users/' + idEmpleado)
            .then(response => {
                // Verificar si la respuesta es exitosa
                if (response.status === 200) {
                    // Obtener los datos del usuario desde la respuesta
                    var user = response.data;

                    // Mostrar los datos en la consola
                    console.log(user);

                    // Desplegar los datos en la página
                    displayUserData(user);
                } else {
                    throw new Error('Error al obtener datos del empleado');
                }
            })
            .catch(error => {
                // Manejar excepciones específicas para códigos de estado del 1 al 10
                if (error.response) {
                    switch (error.response.status) {
                        case 100:
                            console.error('Error 100: Continue');
                            break;
                        // Agrega casos para los demás códigos de estado del 1 al 10 según sea necesario
                        default:
                            console.error('Error inesperado al obtener datos del empleado');
                    }
                } else if (error.request) {
                    console.error('No se recibió respuesta del servidor');
                } else {
                    console.error('Error inesperado: ', error.message);
                }
            });
    } else {
        alert('Por favor, ingresa un ID de empleado válido.');
    }
});

// Función para mostrar los datos en la página
function displayUserData(user) {
    // Limpiar resultados anteriores
    document.getElementById('resultados').innerHTML = '';

    // Crear un nuevo elemento de div
    var div = document.createElement('div');

    // Agregar contenido al div
    div.innerHTML = `
        <label for="nombre">Nombre</label>
        <input type="text" value="${user.name}" disabled>

        <label for="nombreUsuario">Nombre de Usuario</label>
        <input type="text" value="${user.username}" disabled>

        <label for="email">Email</label>
        <input type="text" value="${user.email}" disabled>

        <label for="domicilio">Domicilio</label>
        <input type="text" value="${user.address.street}" disabled>

        <label for="numero">Número</label>
        <input type="text" value="${user.address.suite}" disabled>

        <label for="ciudad">Ciudad</label>
        <input type="text" value="${user.address.city}" disabled>
    `;

    // Agregar el div a la página
    document.getElementById('resultados').appendChild(div);
}
