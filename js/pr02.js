document.getElementById('btnBuscar').addEventListener('click', function() {
    // Obtener el valor del campo de entrada
    var nombrePais = document.getElementById('nombrePais').value;

    // Verificar si el campo de entrada no está vacío
    if (nombrePais.trim() !== '') {
        // Hacer la solicitud a la API con el nombre del país
        fetch(`https://restcountries.com/v3.1/name/${nombrePais}`)
            .then(response => {
                // Verificar si la respuesta es exitosa
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error('Error al obtener datos del país');
                    console.log("Error al obtener datos del país");
                }
            })
            .then(data => {
                // Mostrar los datos en la consola
                console.log(data);

                // Verificar si se encontró algún país
                if (data.length > 0) {
                    // Mostrar la capital, el lenguaje y la bandera
                    displayCountryData(data[0]);
                } else {
                    alert('No se encontró el país.');
                }
            })
            .catch(error => {
                // Manejar excepciones
                if (error.message.includes('NetworkError')) {
                    console.error('Error de red. Verifica tu conexión a Internet.');
                } else if (error.message.includes('TypeError')) {
                    alert('Error al procesar la respuesta de la API. Asegúrate de que los países estén escritos en inglés.');
                } else {
                    console.error(error.message);
                }
            });
    } else {
        alert('Por favor, ingresa el nombre de un país.');
    }
});

// Botón Limpiar
document.getElementById('btnLimpiar').addEventListener('click', function() {
    // Limpiar resultados
    document.getElementById('resultados').innerHTML = '';
});

// Función para mostrar los datos del país en la página
function displayCountryData(country) {
    // Limpiar resultados anteriores
    document.getElementById('resultados').innerHTML = '';

    // Crear un nuevo elemento de div
    var div = document.createElement('div');

    // Verificar si hay datos disponibles
    if (country) {
        // Agregar contenido al div
        div.innerHTML = `
            <h3>${country.name.common}</h3>
            <p>Capital: ${country.capital[0]}</p>
            <p>Lenguaje: ${Object.values(country.languages)[0]}</p>
            <img src="${country.flags.png}" alt="Bandera de ${country.name.common}">
        `;
    } else {
        // Mostrar un mensaje si no hay datos disponibles
        div.innerHTML = '<p>No se encontraron datos para este país.</p>';
    }

    // Agregar el div a la página
    document.getElementById('resultados').appendChild(div);
}
